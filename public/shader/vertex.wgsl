struct Input {
    [[location(0)]] pos: vec3<f32>;
    [[location(1)]] normal: vec3<f32>;
    [[location(2)]] vt: vec2<f32>;
};

struct Output {
    [[builtin(position)]] pos: vec4<f32>;
    [[location(0)]] vt: vec2<f32>;
};

[[stage(vertex)]]
fn main(input: Input) -> Output {
    var output: Output;
    output.pos = vec4<f32>(input.pos, 1.0);
    output.vt = input.vt;
    return output;
};