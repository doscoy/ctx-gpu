[[block]]
struct Parameters {
    res: vec2<f32>;
    mouse: vec2<f32>;
    sec: f32;
};

[[group(0), binding(0)]] var<uniform> params_: Parameters;
[[group(0), binding(1)]] var sampler_: sampler;
[[group(0), binding(2)]] var texture_: texture_2d<f32>;

struct Input {
    [[builtin(position)]] pos: vec4<f32>;
    [[location(0)]] vt: vec2<f32>;
};

let tgt_res = vec2<f32>(600., 400.);
let pix_size = vec2<f32>(1., 2.);
let pix_scale = 0.500;
let lens = 0.250;
let band_w = 0.250;
let band_s = 0.750;
let glitch_s = 5.000;
let glitch_f = 20.000;
let glitch_t = 0.995;
let thr = 0.650;
let range = 0.350;

fn calcLens(vt: vec2<f32>, lens: f32) -> vec2<f32> {
    let center = vec2<f32>(0.5);
    let vector = vt - center;
    let len = length(vector * lens) - lens * 0.5;
    return (vt + vector * len);
}

fn calcBandEffect(vt: vec2<f32>, sec: f32, width: f32, speed: f32) -> f32 {
    let pos = fract(sec * speed) * (1.0 + width * 2.0) - width;
    return (step(pos, vt.y) * step(vt.y, pos + width));
}

fn calcLensMask(lens_vt: vec2<f32>, tgt_res: vec2<f32>) -> f32 {
    let cut = vec2<f32>(0.0, 1.0 - tgt_res.y / tgt_res.x) * 0.5;
    let mask = step(vec2<f32>(0.0), lens_vt - cut) * step(lens_vt + cut, vec2<f32>(1.0));
    return mask.x * mask.y;
}

struct CheckerMosaic {
    vt: vec2<f32>;
    c_index: i32;
    checker: f32;
};

fn calcCheckerMosaic(vt: vec2<f32>, tgt_res: vec2<f32>, pix_size: vec2<f32>, pix_scale: f32) -> CheckerMosaic {
    var tmp: CheckerMosaic;
    let mos_factor = tgt_res * (1.0 / pix_size) * pix_scale;
    let mosaic = floor(vt * mos_factor);
    tmp.vt = mosaic / mos_factor;
    let in_mos_vt = fract(vt * mos_factor);
    tmp.c_index = i32(clamp(0.0, 2.0, in_mos_vt.x * 3.0));
    tmp.checker = (mosaic.x + mosaic.y) % 2.0;
    return tmp;
}

fn rndf (x: f32) -> f32 {
    return fract(sin(dot(vec2<f32>(x), vec2<f32>(12.9898, 78.233))) * 43758.5453123);
}

fn rndv2 (v: vec2<f32>) -> vec2<f32> {
    return vec2<f32>(rndf(v.x), rndf(v.y));
}

fn calcGlitchEffect(vt: vec2<f32>, sec: f32, speed: f32, freq: f32, thr: f32) -> vec2<f32> {
    var base = vec2<f32>(sec * speed);
    base.x = base.x + (1.0 - vt.y) * freq;
    let i = floor(base);
    let f = fract(base);
    let r = mix(rndv2(i), rndv2(i + 1.0), smoothStep(vec2<f32>(0.0), vec2<f32>(1.0), f));
    return (step(thr, sin(tan(sec) * sec)) * (r - 0.5) + (r - 0.5) * 0.010) * 0.5;
}

fn getPixel(vt: vec2<f32>) -> vec4<f32> {
    let lens_vt = calcLens(vt, lens);
    let band = calcBandEffect(lens_vt, params_.sec, band_w, band_s);
    let mask = calcLensMask(lens_vt, tgt_res);
    let cm = calcCheckerMosaic(lens_vt, tgt_res, pix_size, pix_scale);
    let glitch = calcGlitchEffect(vt, params_.sec, glitch_s, glitch_f, glitch_t);
    let tex_color = textureSample(texture_, sampler_, fract(cm.vt + glitch));
    var out_color = vec4<f32>(0.0, 0.0, 0.0, 1.0);
    out_color[cm.c_index] = step(thr + range * (cm.checker - 0.5), tex_color[cm.c_index]);
    return (out_color - band * vec4<f32>(0.21, 0.72, 0.07, 0.0) * 0.50) * mask;
}

fn cheapBlur(vt: vec2<f32>, res: vec2<f32>) -> vec4<f32> {
    let pix_dist = 1.0 / res;
    let grow = 0.25;
    var acum = getPixel(vt) * 0.8;
    acum = acum + getPixel(vt + vec2<f32>( pix_dist.x,  pix_dist.y)) * grow;
    acum = acum + getPixel(vt + vec2<f32>( pix_dist.x, -pix_dist.y)) * grow;
    acum = acum + getPixel(vt + vec2<f32>(-pix_dist.x,  pix_dist.y)) * grow;
    acum = acum + getPixel(vt + vec2<f32>(-pix_dist.x, -pix_dist.y)) * grow;
    return acum;
}

[[stage(fragment)]]
fn main(input: Input) -> [[location(0)]] vec4<f32> {
    return cheapBlur(input.vt, params_.res);
};