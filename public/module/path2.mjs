export function path2ImageData(path) {
  return new Promise(resolve => {
    const img = new Image();
    img.addEventListener("load", () =>{
      const cv = document.createElement("canvas");
      cv.width = img.width;
      cv.height = img.height;
      const ctx = cv.getContext("2d");
      ctx.drawImage(img, 0, 0);
      const data = ctx.getImageData(0, 0, cv.width, cv.height);
      resolve(data);
    });
    img.src = path;
  });
}

export function path2TextData(path) {
  return fetch(path).then(response => response.text());
}