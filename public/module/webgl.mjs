///////////////
// read only //
///////////////

export default class Renderer {
    #resource;

    #context;

    #shader = {
        vertex: {
            enable: false,
            module: null,
            log: null
        },
        fragment: {
            enable: false,
            module: null,
            log: null
        }
    };

    #program = {
        enable: false,
        module: null,
        log: null
    };

    #vertex_buffer = {
        vertex: {
            buffer: null,
            stride: null,
            ident: [],
            attribute: [],
            format: [],
            size: [],
            offset: []
        },
        index: {
            buffer: null,
            format: null,
            length: 0
        }
    };

    #uniform = {
        ident: [],
        location: []
    };

    #texture = {
        ident: null,
        buffer: null,
        location: null
    };

    constructor(resource) { this.#resource = resource; }

    checkAPIEnable() { return this.#resource.getContext("webgl") !== null; }

    async init(init_sources) {
        this.#setRenderResource();
        this.#setShaderModule(init_sources);
        this.#setProgramModule();
        this.#setVertexBuffer();
        this.#setUniform();
        this.#setTexture();
        if (!this.#shader.vertex.enable ||
            !this.#shader.fragment.enable ||
            !this.#program.enable) {
            return this.#shader.vertex.log +
                   this.#shader.fragment.log +
                   this.#program.log;
        }
        this.#setLocation();
        return null
    }

    draw() {
        this.#context.clear(this.#context.COLOR_BUFFER_BIT);
        this.#context.useProgram(this.#program.module);
        this.#context.activeTexture(this.#context.TEXTURE0);
        this.#context.bindTexture(this.#context.TEXTURE_2D, this.#texture.buffer);
        this.#context.uniform1i(this.#texture.location, 0);
        this.#updateUniform();
        this.#context.bindBuffer(
            this.#context.ARRAY_BUFFER,
            this.#vertex_buffer.vertex.buffer
        );
        for (let i = 0; i < this.#vertex_buffer.vertex.ident.length; ++i) {
            if (this.#vertex_buffer.vertex.attribute[i] < 0) {
                continue;
            }
            this.#context.vertexAttribPointer(
                this.#vertex_buffer.vertex.attribute[i],
                this.#vertex_buffer.vertex.size[i],
                this.#vertex_buffer.vertex.format[i],
                false,
                this.#vertex_buffer.vertex.stride,
                this.#vertex_buffer.vertex.offset[i]
            );
            this.#context.enableVertexAttribArray(
                this.#vertex_buffer.vertex.attribute[i]
            );
        }
        this.#context.bindBuffer(
            this.#context.ELEMENT_ARRAY_BUFFER,
            this.#vertex_buffer.index.buffer
        );
        this.#context.drawElements(
            this.#context.TRIANGLES,
            this.#vertex_buffer.index.length,
            this.#vertex_buffer.index.format,
            0
        );
        this.#context.flush();
        this.#context.bindBuffer(this.#context.ARRAY_BUFFER, null);
        this.#context.bindBuffer(this.#context.ELEMENT_ARRAY_BUFFER, null);
    }

    async updateShader(src, type) {
        let target = null;
        switch (type) {
            case "vertex":
                type = this.#context.VERTEX_SHADER;
                target = "vertex";
                break;
            case "fragment":
                type = this.#context.FRAGMENT_SHADER;
                target = "fragment";
                break;
            default:
                return undefined;
        }
        const shdr_tmp = this.#createShaderModule(src, type);
        if (!shdr_tmp.enable) {
            return shdr_tmp.log;
        }
        this.#shader[target] = shdr_tmp;
        const prog_tmp = this.#createProgramModule();
        if (!prog_tmp.enable) {
            return prog_tmp.log;
        }
        this.#program = prog_tmp;
        this.#setLocation();
        return null;
    }

    #setRenderResource() {
        this.#context = this.#resource.getContext("webgl");
        this.#context.clearColor(0.0, 0.0, 0.0, 1.0);
    }

    #setShaderModule(sources) {
        this.#shader.vertex =
            this.#createShaderModule(sources.vertex, this.#context.VERTEX_SHADER);
        this.#shader.fragment =
            this.#createShaderModule(sources.fragment, this.#context.FRAGMENT_SHADER);
    }

    #createShaderModule(src, type) {
        const tmp = {};
        tmp.module = this.#context.createShader(type);
        this.#context.shaderSource(tmp.module, src);
        this.#context.compileShader(tmp.module);
        if (this.#context.getShaderParameter(tmp.module, this.#context.COMPILE_STATUS)) {
            tmp.enable = true;
            tmp.log = "";
        } else {
            tmp.enable = false;
            tmp.log = this.#context.getShaderInfoLog(tmp.module);
        }
        return tmp;
    }

    #setProgramModule() {
        this.#program = this.#createProgramModule();
    }

    #createProgramModule() {
        const tmp = {};
        tmp.module = this.#context.createProgram();
        this.#context.attachShader(tmp.module, this.#shader.vertex.module);
        this.#context.attachShader(tmp.module, this.#shader.fragment.module);
        this.#context.linkProgram(tmp.module);
        if (this.#context.getProgramParameter(tmp.module, this.#context.LINK_STATUS)) {
            tmp.enable = true;
        } else {
            tmp.enable = false;
            tmp.log = this.#context.getProgramInfoLog(tmp.module);
        }
        return tmp;
    }

    #setVertexBuffer() {
        this.#vertex_buffer.vertex.buffer = this.#context.createBuffer();
        const vertices = new Float32Array([
            //  posision xyz,    normal xyz,    vt xy
            -1.0, -1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
            +1.0, -1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0,
            +1.0, +1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0,
            -1.0, +1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0
        ]);
        this.#vertex_buffer.vertex.stride = 4 * (3 + 3 + 2);
        this.#vertex_buffer.vertex.ident.push("position_");
        this.#vertex_buffer.vertex.format.push(this.#context.FLOAT);
        this.#vertex_buffer.vertex.size.push(3);
        this.#vertex_buffer.vertex.offset.push(0);
        this.#vertex_buffer.vertex.ident.push("normal_");
        this.#vertex_buffer.vertex.format.push(this.#context.FLOAT);
        this.#vertex_buffer.vertex.size.push(3);
        this.#vertex_buffer.vertex.offset.push(4 * 3);
        this.#vertex_buffer.vertex.ident.push("vt_");
        this.#vertex_buffer.vertex.format.push(this.#context.FLOAT);
        this.#vertex_buffer.vertex.size.push(2);
        this.#vertex_buffer.vertex.offset.push(4 * (3 + 3));
        this.#context.bindBuffer(
            this.#context.ARRAY_BUFFER,
            this.#vertex_buffer.vertex.buffer
        );
        this.#context.bufferData(
            this.#context.ARRAY_BUFFER,
            vertices,
            this.#context.STATIC_DRAW
        );

        this.#vertex_buffer.index.buffer = this.#context.createBuffer();
        const indices = new Int8Array([
            0, 1, 2,
            0, 2, 3
        ]);
        this.#vertex_buffer.index.format = this.#context.UNSIGNED_BYTE;
        this.#vertex_buffer.index.length = indices.length;
        this.#context.bindBuffer(
            this.#context.ELEMENT_ARRAY_BUFFER,
            this.#vertex_buffer.index.buffer
        );
        this.#context.bufferData(
            this.#context.ELEMENT_ARRAY_BUFFER,
            indices,
            this.#context.STATIC_DRAW
        );

        this.#context.bindBuffer(this.#context.ARRAY_BUFFER, null);
        this.#context.bindBuffer(this.#context.ELEMENT_ARRAY_BUFFER, null);
    }

    #setUniform() {
        this.#uniform.ident = ["res_", "mouse_", "sec_"];
    }

    #setTexture() {
        this.#texture.ident = "texture_";
        this.#texture.buffer = this.#context.createTexture();
        this.#context.pixelStorei(this.#context.UNPACK_FLIP_Y_WEBGL, true);
        this.#context.bindTexture(this.#context.TEXTURE_2D, this.#texture.buffer);
        this.#context.texImage2D(
            this.#context.TEXTURE_2D,
            0,
            this.#context.RGBA,
            this.#resource.img.width,
            this.#resource.img.height,
            0,
            this.#context.RGBA,
            this.#context.UNSIGNED_BYTE,
            this.#resource.img.data
        );
        this.#context.texParameteri(
            this.#context.TEXTURE_2D,
            this.#context.TEXTURE_MAG_FILTER,
            this.#context.LINEAR
        );
        this.#context.texParameteri(
            this.#context.TEXTURE_2D,
            this.#context.TEXTURE_MIN_FILTER,
            this.#context.LINEAR
        );
        this.#context.texParameteri(
            this.#context.TEXTURE_2D,
            this.#context.TEXTURE_WRAP_S,
            this.#context.REPEAT
        );
        this.#context.texParameteri(
            this.#context.TEXTURE_2D,
            this.#context.TEXTURE_WRAP_T,
            this.#context.REPEAT
        );
        this.#context.bindTexture(this.#context.TEXTURE_2D, null);
        this.#context.pixelStorei(this.#context.UNPACK_FLIP_Y_WEBGL, false);
    }

    #setLocation() {
        this.#vertex_buffer.vertex.attribute = new Array();
        for (const ident of this.#vertex_buffer.vertex.ident) {
            this.#vertex_buffer.vertex.attribute.push(
                this.#context.getAttribLocation(this.#program.module, ident)
            );
        }
        this.#texture.location =
            this.#context.getUniformLocation(
                this.#program.module,
                this.#texture.ident
            );
        this.#uniform.location = new Array();
        for (const ident of this.#uniform.ident) {
            this.#uniform.location.push(
                this.#context.getUniformLocation(this.#program.module, ident)
            );
        }
    }

    #updateUniform() {
        this.#context.uniform2f(
            this.#uniform.location[0],
            this.#resource.canvasRes.width,
            this.#resource.canvasRes.height
        );
        this.#context.uniform2f(
            this.#uniform.location[1],
            this.#resource.mouse.x,
            this.#resource.mouse.y
        );
        this.#context.uniform1f(
            this.#uniform.location[2],
            this.#resource.time
        );
    }
};