///////////////
// read only //
///////////////

export default class Renderer {
    #resource;

    #adapter;
    #device;
    #context;

    #shader = {
        vertex: {
            enable: false,
            module: null,
            log: null
        },
        fragment: {
            enable: false,
            module: null,
            log: null
        }
    };

    #vertex_buffer = {
        vertex: {
            buffer: null,
            stride: null,
            attributes: []
        },
        index: {
            buffer: null,
            format: null,
            length: 0
        }
    };

    #uniform = {
        buffer: null,
        size: 0,
    };

    #texture = {
        texture: null,
        sampler: null
    };

    #bind_group = {
        layout: null,
        bind_group: null
    };

    #render_pipeline = {
        enable: false,
        module: null,
        log: null
    }

    constructor(resource) { this.#resource = resource; }

    checkAPIEnable() {
        if (!navigator.gpu) {
            return false;
        } else if (!navigator.gpu.requestAdapter()) {
            return false;
        }
        return true;
    }

    async init(init_sources) {
        await this.#setRenderResource();
        await this.#setShaderModule(init_sources);
        this.#setVertexBuffer();
        this.#setUniform();
        this.#setTexture();
        this.#setBindGroup();
        await this.#setRenderPipeline();
        if (this.#shader.vertex.enable &&
            this.#shader.fragment.enable &&
            this.#render_pipeline.enable) {
            return null;
        } else {
            return this.#shader.vertex.log +
                   this.#shader.fragment.log +
                   this.#render_pipeline.log;
        }
    }

    draw() {
        this.#updateUniform();
        const render_pass_desc = {
            colorAttachments: [{ view: undefined, loadValue: [0.0, 0.0, 0.0, 1.0] }]
        };
        render_pass_desc.colorAttachments[0].view = this.#context.getCurrentTexture().createView();
        const command_encoder = this.#device.createCommandEncoder();
        const render_pass = command_encoder.beginRenderPass(render_pass_desc);
        render_pass.setPipeline(this.#render_pipeline.module);
        render_pass.setIndexBuffer(this.#vertex_buffer.index.buffer, this.#vertex_buffer.index.format);
        render_pass.setVertexBuffer(0, this.#vertex_buffer.vertex.buffer);
        render_pass.setBindGroup(0, this.#bind_group.bind_group);
        render_pass.drawIndexed(this.#vertex_buffer.index.length);
        render_pass.endPass();
        this.#device.queue.submit([command_encoder.finish()]);
    }

    async updateShader(src, type) {
        if (type !== "vertex" && type !== "fragment") {
            return undefined;
        }
        const shdr_tmp = await this.#createShaderModule(src);
        if (!shdr_tmp.enable) {
            return shdr_tmp.log;
        }
        this.#shader[type] = shdr_tmp;
        const rndr_tmp = await this.#createRenderPipeline();
        if (!rndr_tmp.enable) {
            return rndr_tmp.log;
        }
        this.#render_pipeline = rndr_tmp;
        return null;
    }

    async #setRenderResource() {
        this.#adapter = await navigator.gpu.requestAdapter();
        this.#device = await this.#adapter.requestDevice();
        this.#context = this.#resource.getContext("webgpu");
        this.#context.configure({
            device: this.#device,
            format: this.#context.getPreferredFormat(this.#adapter),
            usage: GPUTextureUsage.RENDER_ATTACHMENT
        });
    }

    async #setShaderModule(sources) {
        this.#shader.vertex = await this.#createShaderModule(sources.vertex);
        this.#shader.fragment = await this.#createShaderModule(sources.fragment);
    }

    async #createShaderModule(src) {
        const tmp = {};
        tmp.module = this.#device.createShaderModule({ code: src });
        tmp.log = "";
        let has_error = false;
        if (tmp.module.compilationInfo) {
            const info = await tmp.module.compilationInfo();
            if (info.messages.length > 0) {
                for (const msg of info.messages) {
                    tmp.log += `${msg.lineNum}:${msg.linePos} - ${msg.message}\n`;
                    has_error = has_error || msg.type == "error";
                }
            }
        }
        tmp.enable = !has_error;
        return tmp;
    }

    #setVertexBuffer() {
        const vertex = [
        //  posision xyz,    normal xyz,    vt xy
            -1.0, -1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0,
            +1.0, -1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0,
            +1.0, +1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0,
            -1.0, +1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0
        ];
        this.#vertex_buffer.vertex.buffer = this.#device.createBuffer({
            size: 4 * vertex.length,
            usage: GPUBufferUsage.VERTEX,
            mappedAtCreation: true
        });
        new Float32Array(this.#vertex_buffer.vertex.buffer.getMappedRange()).set(vertex);
        this.#vertex_buffer.vertex.buffer.unmap();
        this.#vertex_buffer.vertex.stride = 4 * (3 + 3 + 2);
        this.#vertex_buffer.vertex.attributes.push({
            format: "float32x3",
            offset: 0,
            shaderLocation: 0
        });
        this.#vertex_buffer.vertex.attributes.push({
            format: "float32x3",
            offset: 4 * 3,
            shaderLocation: 1
        });
        this.#vertex_buffer.vertex.attributes.push({
            format: "float32x2",
            offset: 4 * (3 + 3),
            shaderLocation: 2
        });

        const index = [
            0, 1, 2,
            0, 2, 3
        ];
        this.#vertex_buffer.index.buffer = this.#device.createBuffer({
            size: 4 * index.length,
            usage: GPUBufferUsage.INDEX,
            mappedAtCreation: true
        });
        new Uint32Array(this.#vertex_buffer.index.buffer.getMappedRange()).set(index);
        this.#vertex_buffer.index.buffer.unmap();
        this.#vertex_buffer.index.format = "uint32";
        this.#vertex_buffer.index.length = index.length;
    }

    #setUniform() {
        this.#uniform.size = (4 * 2) + (4 * 2) + (4 * 1);
        this.#uniform.buffer = this.#device.createBuffer({
            size: this.#uniform.size,
            usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST
        });
    }

    #setTexture() {
        this.#texture.sampler = this.#device.createSampler({
            addressModeU: "repeat",
            addressModeV: "repeat",
            magFilter: "linear",
            minFilter: "linear",
        });

        const size = {
            width: this.#resource.img.width,
            height: this.#resource.img.height,
            depth: 1
        }
        this.#texture.texture = this.#device.createTexture({
            size: size,
            format: "rgba8unorm",
            usage: GPUTextureUsage.TEXTURE_BINDING |
                GPUTextureUsage.COPY_DST |
                GPUTextureUsage.RENDER_ATTACHMENT
        });
        this.#device.queue.writeTexture(
            { texture: this.#texture.texture },
            this.#resource.img.data,
            { bytesPerRow: size.width * 4, rowsPerImage: size.height },
            size
        );
    }

    #setBindGroup() {
        this.#bind_group.layout = this.#device.createBindGroupLayout({
            entries: [{
                binding: 0,
                visibility: GPUShaderStage.VERTEX | GPUShaderStage.FRAGMENT,
                buffer: { type: "uniform" },
            }, {
                binding: 1,
                visibility: GPUShaderStage.FRAGMENT,
                sampler: { type: "filtering" },
            }, {
                binding: 2,
                visibility: GPUShaderStage.FRAGMENT,
                texture: { sampleType: "float" }
            }
            ]
        });

        this.#bind_group.bind_group = this.#device.createBindGroup({
            layout: this.#bind_group.layout,
            entries: [{
                binding: 0,
                resource: { buffer: this.#uniform.buffer }
            }, {
                binding: 1,
                resource: this.#texture.sampler
            }, {
                binding: 2,
                resource: this.#texture.texture.createView()
            }
            ]
        });
    }

    async #setRenderPipeline() {
        this.#render_pipeline = await this.#createRenderPipeline();
    }

    async #createRenderPipeline() {
        const layout =
            this.#device.createPipelineLayout({
                bindGroupLayouts: [this.#bind_group.layout]
            });
        this.#device.pushErrorScope("validation");
        const tmp = {}
        tmp.module = this.#device.createRenderPipeline({
            layout: layout,
            vertex: {
                module: this.#shader.vertex.module,
                entryPoint: "main",
                buffers: [{
                    arrayStride: this.#vertex_buffer.vertex.stride,
                    attributes: this.#vertex_buffer.vertex.attributes
                }]
            },
            fragment: {
                module: this.#shader.fragment.module,
                entryPoint: "main",
                targets: [{
                    format: this.#context.getPreferredFormat(this.#adapter)
                }]
            }
        });
        const err = await this.#device.popErrorScope();
        if (err === null) {
            tmp.enable = true;
            tmp.log = "";
        } else {
            tmp.enable = false;
            tmp.log = err.message;
        }
        return tmp;
    }

    #updateUniform() {
        const data = new Float32Array([
            this.#resource.canvasRes.width, this.#resource.canvasRes.height,
            this.#resource.mouse.x, this.#resource.mouse.y,
            this.#resource.time
        ]);
        this.#device.queue.writeBuffer(this.#uniform.buffer, 0, data);
    }
};