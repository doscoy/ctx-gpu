export default class BaseResource {
  #canvas;
  #img;
  #pixel_ratio;
  #resolution = {
    width: 0,
    height: 0
  };
  #time;
  #mouse_position = {
    x: 0.0,
    y: 0.0
  };
  constructor(canvas_id, img_data) {
    this.#canvas = document.getElementById(canvas_id);
    this.#img = img_data;
    this.#pixel_ratio = window.devicePixelRatio || 1;
    this.#initCanvas();
    this.stopTimer();
  }
  get canvasRes() { return this.#resolution };
  get img() { return this.#img; }
  get time() {
    if (this.#time === null) {
      return 0.0;
    } else {
      return (performance.now() - this.#time) / 1000.0;
    }
  }
  get mouse() { return this.#mouse_position; }

  getContext(type, ...args) { return this.#canvas.getContext(type, ...args); }
  resetCanvas() {
    const new_canvas = document.createElement("canvas");
    new_canvas.id = this.#canvas.id;
    this.#canvas.parentNode.replaceChild(new_canvas, this.#canvas);
    this.#canvas = new_canvas;
    this.#initCanvas();
  }
  startTimer() { this.#time = performance.now(); }
  stopTimer() { this.#time = null; }
  #initCanvas() {
    this.#resolution.width = 
      this.#canvas.getBoundingClientRect().width * this.#pixel_ratio;
    this.#resolution.height =
      this.#canvas.getBoundingClientRect().height * this.#pixel_ratio;
    this.#canvas.width = this.#resolution.width;
    this.#canvas.height = this.#resolution.height;
    this.#canvas.addEventListener("mousemove", e => this.#updateMousePos(e));
  }
  #updateMousePos(e) {
    this.#mouse_position.x = e.offsetX * this.#pixel_ratio;
    this.#mouse_position.y = e.offsetY * this.#pixel_ratio;
  }
};