import { path2ImageData, path2TextData } from "./path2.mjs";
import BaseResource from "./base_resource.mjs";

const MODE = {
  WebGL: {
    js: (new URL("./module/webgl.mjs", location.href)).href,
    vertex: (new URL("./shader/vertex.glsl100", location.href)).href,
    fragment: (new URL("./shader/fragment.glsl100", location.href)).href
  },
  WebGL2: {
    js: (new URL("./module/webgl2.mjs", location.href)).href,
    vertex: (new URL("./shader/vertex.glsl300es", location.href)).href,
    fragment: (new URL("./shader/fragment.glsl300es", location.href)).href
  },
  WebGPU: {
    js: (new URL("./module/webgpu.mjs", location.href)).href,
    vertex: (new URL("./shader/vertex.wgsl", location.href)).href,
    fragment: (new URL("./shader/fragment.wgsl", location.href)).href
  }
}

export default class App {
  #resource;
  #sources = {};
  #renderer;
  #msg;
  #editor = {
    header: document.createElement("div"),
    play: document.createElement("button"),
    mode: document.createElement("select"),
    src: document.createElement("select"),
    cur_pos: document.createElement("span"),
    text: document.createElement("div"),
    ace: null
  }
  #animation_id = null;
  #callback = {
    updateSrc: () => this.#updateSrc()
  }

  constructor() {
    for (const mode in MODE) {
      this.#sources[mode] = {};
      for (const src in MODE[mode]) {
        this.#sources[mode][src] = "";
      }
      this.#sources[mode]["loaded"] = false;
      this.#sources[mode]["valid"] = false;
    }

    this.#editor.header.id = "header";

    this.#editor.play.classList.add("button");
    this.#editor.play.innerText = "play/stop";
    this.#editor.play.addEventListener("click", () => this.#pushPlayButton());
    this.#editor.header.appendChild(this.#editor.play);

    this.#editor.mode.classList.add("list");
    for (const list in MODE) {
      const opt = document.createElement("option");
      opt.value = opt.innerText = list;
      this.#editor.mode.appendChild(opt);
    }
    this.#editor.mode.addEventListener("change", () => this.#modeListChange());
    this.#editor.header.appendChild(this.#editor.mode);

    this.#editor.src.classList.add("list");
    for (const list in Object.entries(MODE)[0][1]) {
      const opt = document.createElement("option");
      opt.value = opt.innerText = list;
      this.#editor.src.appendChild(opt);
    }
    this.#editor.src.addEventListener("change", () => this.#srcListChange());
    this.#editor.header.appendChild(this.#editor.src);

    this.#editor.cur_pos.id = "cursor_position";
    this.#editor.header.appendChild(this.#editor.cur_pos);

    this.#editor.text.id = "text";
  }

  async init(canvas_id, msg_id, editor_id, img_path) {
    this.#resource = new BaseResource(canvas_id, await path2ImageData(img_path));
    this.#msg = document.getElementById(msg_id);
    const div = document.getElementById(editor_id);
    div.appendChild(this.#editor.header);
    div.appendChild(this.#editor.text);
    this.#editor.ace = ace.edit(this.#editor.text, {
      theme: "ace/theme/twilight",
      enableBasicAutocompletion: true,
      enableLiveAutocompletion: true,
      enableWheelValue: true,
      showPrintMargin: false,
      useSoftTabs: true,
      tabSize: 4
    });
    this.#editor.ace.on("changeSelection", () =>{
      const pos = this.#editor.ace.getCursorPosition();
      this.#editor.cur_pos.innerText = (pos.row + 1) + ":" + (pos.column + 1);
    });
    this.#animation_id = undefined;
    this.#modeListChange();
  }

  #checkRendererEnable() { return this.#renderer !== undefined || this.#renderer !== null; }

  async set(mode) {
    const is_playing = this.#animation_id !== null;
    this.#stop();
    if (!this.#sources[mode].loaded) {
      this.#sources[mode].js = await path2TextData(MODE[mode].js);
      this.#sources[mode].vertex = await path2TextData(MODE[mode].vertex);
      this.#sources[mode].fragment = await path2TextData(MODE[mode].fragment);
      this.#sources[mode].loaded = true;
    }
    this.#srcListChange();
    this.#resource.resetCanvas();
    const module = await import(MODE[mode].js);
    this.#renderer = new module.default(this.#resource);
    if (!this.#renderer.checkAPIEnable()) {
      this.#renderer = null;
      this.#msg.innerText = "can't use this browser";
      return;
    }
    const log = await this.#renderer.init(this.#sources[mode]);
    if (log !== null) {
      this.#msg.innerText = log;
    } else if (is_playing) {
      this.#play();
    } else {
      this.#renderer.draw();
    }
  }

  #play() {
    const frame = () => {
      this.#renderer.draw();
      this.#msg.innerText = `rendering\nmouse: ${this.#resource.mouse.x}, ${this.#resource.mouse.y}\ntime : ${this.#resource.time.toFixed(2)}`;
      this.#animation_id = requestAnimationFrame(frame);
    };
    this.#animation_id = undefined;
    this.#editor.play.innerText = "stop";
    this.#resource.startTimer();
    frame();
  }

  #stop() {
    cancelAnimationFrame(this.#animation_id);
    this.#editor.play.innerText = "play";
    this.#msg.innerText = "stop";
    this.#animation_id = null;
    this.#resource.stopTimer();
  }

  #pushPlayButton() {
    if (!this.#checkRendererEnable()) {
      return;
    }
    if (this.#animation_id === null) {
      this.#play();
    } else {
      this.#stop();
    }
  }

  #modeListChange() { this.set(this.#editor.mode.value); }

  #srcListChange() {
    this.#editor.ace.setValue(this.#sources[this.#editor.mode.value][this.#editor.src.value]);
    this.#editor.ace.moveCursorTo(0, 0);
    this.#editor.ace.scrollToLine(0, false, false);
    if (this.#editor.src.value === "js") {
      this.#editor.ace.setReadOnly(true);
      this.#editor.ace.session.off("tokenizerUpdate", this.#callback.updateSrc);
      this.#editor.ace.session.setMode("ace/mode/javascript");
    } else {
      this.#editor.ace.setReadOnly(false);
      this.#editor.ace.session.on("tokenizerUpdate", this.#callback.updateSrc);
      const lang = MODE[this.#editor.mode.value][this.#editor.src.value].split('.').pop();
      this.#editor.ace.session.setMode("ace/mode/" + lang);
    }
  }

  #updateSrc() {
    this.#sources[this.#editor.mode.value][this.#editor.src.value] = this.#editor.ace.getValue();
    if (!this.#checkRendererEnable()) {
      return;
    } else if (this.#editor.src.value !== "js") {
      const type = this.#editor.src.value;
      this.#renderer.updateShader(this.#sources[this.#editor.mode.value][type], type).then(msg => {
        if (msg === undefined) {
          return;
        } else if (msg !== null) {
          this.#stop();
          this.#msg.innerText = msg;
        } else if (this.#animation_id === null) {
          this.#msg.innerText = "compilation succeeded!!";
          this.#renderer.draw();
        }
      });
    }
  }
};
