# ctx-gpu

WebGL、WebGL2、WebGPUの比較

https://doscoy.gitlab.io/ctx-gpu

※WebGPUの実行には対応ブラウザとフラグ変更が必要
（Google Chrome Canary -> --enable unsafe-webgpu）